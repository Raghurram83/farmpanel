import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {  JsonpModule } from '@angular/http';
import {ToastyModule} from 'ng2-toasty';
import { NgxGalleryModule } from 'ngx-gallery';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';
import {MenuItems} from './shared/menu-items/menu-items';
import {BreadcrumbsComponent} from './layout/admin/breadcrumbs/breadcrumbs.component';
import { TestingComponent } from './theme/testing/testing.component';
import { TestComponent } from './test/test.component';

import { FileuploadService } from './fileupload.service';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';

import { FlatpickrModule } from 'angularx-flatpickr';

import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import { LoginComponent } from './login/login.component';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {SelectModule} from 'ng-select';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { CategoryComponent } from './category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { ProductComponent } from './product/product.component';
import { OfferComponent } from './offer/offer.component';
import { BannerComponent } from './banner/banner.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent,
    TestingComponent,
    TestComponent,

    DashboardComponent,
    
    LoginComponent,
    
   
    CategoryComponent,
    SubcategoryComponent,
    ProductComponent,
    OfferComponent,
    BannerComponent

  
  ],
  imports: [
    CommonModule,
    BrowserModule,
    SelectModule,
    BrowserAnimationsModule,
    NgbModalModule,
    AppRoutingModule,
    SharedModule,
    HttpModule,
    FormsModule,
    JsonpModule,
    NgxDatatableModule,
    FlatpickrModule.forRoot(),
    ToastyModule.forRoot(),
    IonicStorageModule.forRoot(),
    NgxGalleryModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    

  ],
  providers: [MenuItems,
    
    FileuploadService,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
