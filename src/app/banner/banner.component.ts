import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import { ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';  
import {animate, style, transition, trigger} from '@angular/animations';
import {Http, Headers } from '@angular/http';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';
import PNotify from "pnotify/dist/es/PNotify";
import PNotifyButtons from "pnotify/dist/es/PNotifyButtons";
import PNotifyConfirm from "pnotify/dist/es/PNotifyConfirm";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import swal from 'sweetalert2';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../global.service';
import { FileuploadService } from '../fileupload.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss',
  '../../assets/icon/icofont/css/icofont.scss'

],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
  })
export class BannerComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild("modalLarge") modal: any;
  @ViewChild("modalForm") modal1: any;
      
  showDialog = false;
  @Input() visible: boolean;
  public config: any;

  editProfile = true;
  editProfileIcon = 'icofont-edit';
  image:any;
  loaded:any;
  dumy:any={};
  selectedFile: any;
  imagepreview: any;
  userid:any;
  engineerid:any;
  old:any={};
  s3details:any;
  upload:any;
  readThis:any;
  editAbout = true;
  editAboutIcon = 'icofont-edit';
  
  usertype:any;
  public editor;
  public editorContent: string;

  public data: any;
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  profitChartOption: any;

  rowsContact = [];
  loadingIndicator = true;
  reorderable = true;


  schoolinfodata:any={};
  schoolinfoform:any;
  schoolinfopost:any;
  schoolinfodataloaded:any='0';

  schoolclass:any={};

  addingdata:any;

  position = 'bottom-right';
  title: string;
  msg: string;
  showClose = true;
  theme = 'bootstrap';
  type = 'default';
  closeOther = false;
schoolid:any;

///////////assign trainers form /////////
trainersloaded:any;

assigntrainerloaded:any;
assigntrainerdata:any={};
req:any={};
markasdata:any={};
formdata:any={};
addformdata:any={};
requirementt:any;
userselect:any=[];
///////////////studentlist///////
studentlistdata:any;
studentlistloaded:any;

studentlisttempFilter=[];
 studentlistrowsFilter=[];
 studentlistcolumns = [
  
   
  { name: 'Id',prop:'id' },  
  { name: 'Title',prop:'title' },  
  { name: 'Clinet',prop:'clientname' },  
  { name: 'City',prop:'city' },  
  { name: 'Area',prop:'area' },  
  { name: 'Value',prop:'value' },  
  { name: 'Category',prop:'category' } 
  
];




rowsBasic = [];
  fullScreenRow = [];

  columns = [
  
   
    { name: 'Id',prop:'id',width: 10},  
    { name: 'Name',prop:'name' },  
    { name: 'City',prop:'city' },  
    { name: 'Designation',prop:'designation' },  
    { name: 'Location',prop:'location' },  
    { name: 'Mobile',prop:'mobile' },  
    { name: 'Reports Collected',prop:'"12"' },  
    { name: 'action',prop:'status' },  
  ];

  rows = [];
  expanded = {};
  timeout: any;

   tempFilter=[];
 rowsFilter=[];
 rowsFilter1=[];
 priority=[];
 workersdata:any;
 assigndata:any;
 unassigndata:any;
 unassignloaded:any;

  constructor(private route: ActivatedRoute, public global:GlobalService, private router: Router,public http: Http,private toastyService: ToastyService,public storage: Storage,private fileupload: FileuploadService) {
    this.schoolid = this.route.snapshot.paramMap.get('id');
    this.addformdata.projectid=this.schoolid;
    PNotifyButtons; // Initiate the module. Important!
    PNotifyButtons;
    PNotifyConfirm;
    this.fetchContactData((data) => {
      this.rowsContact = data;
      setTimeout(() => { this.loadingIndicator = false; }, 1500);
    });
  }
  
  update() {
    
   
    
      var link = this.global.serverurl+'/banner/update.php'; 
      
       var myData = JSON.stringify({data:  this.formdata});
       console.log(myData);
       
      
       
       
       
      this.http.post(link,myData)
      //this.http.get(link)
      
       .subscribe(data => {
        console.log(data);
    
       this.data = data["_body"];
    //alert(this.data);
    console.log(this.data);
    
       this.data = JSON.parse(this.data);
    console.log(this.data);
    if(this.data.status=='success'){ 
      this.modal.hide();
  
      swal(
        'Updated!',
        'Details Updated successfully .',
        'success'
      );
    this.ngOnInit();
    }
    
       }, error => {
    this.data.status='failed';
       
    
       });
       } 
    
       
  addtask() {
    if(this.formdata.name==null || this.formdata.offer==null || this.formdata.priority=="" || this.formdata.imagedata==""){
    swal(
      'Failed!',
      'Full all fields .',
      'error'
    );
  return;}
    this.formdata.companyid=this.userid;
      var link = this.global.serverurl+'/banner/addnew.php'; 
      
       var myData = JSON.stringify({data:  this.formdata});
       console.log(myData);
       
      
       
       
       
      this.http.post(link,myData)
      //this.http.get(link)
      
       .subscribe(data => {
        console.log(data);
    
       this.addingdata = data["_body"];
    //alert(this.data);
    console.log(this.addingdata);
    
       this.addingdata = JSON.parse(this.addingdata);
    console.log(this.addingdata);
    if(this.addingdata.status=='success'){ 
      this.addformdata={};
      this.formdata={};
         this.ngOnInit();

  
      swal(
        'Success!',
        'New Announcement Added .',
        'success'
      );
    }
    
       }, error => {
       
    
       });
       } 
    
 
     
  addToast(options) {
    if (options.closeOther) {
      this.toastyService.clearAll();
    }
    this.position = options.position ? options.position : this.position;
    const toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: options.timeout,
      theme: options.theme,
      onAdd: (toast: ToastData) => {
        /* added */
      },
      onRemove: (toast: ToastData) => {
        /* removed */
      }
    };
    this.toastyService.default(toastOptions);
    switch (options.type) {
      case 'default': this.toastyService.default(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }
 
  onFileChanged($event) {

    this.selectedFile = $event.target.files[0];
    
  this.upload=0;
  this.fileupload.uploadfile(this.selectedFile,'trainer/').then( res => {
  console.log(res);
  this.upload=1;

  this.s3details=res;
  this.formdata.imagedata=res;
  this.formdata.url=this.formdata.imagedata.Location;
  

  }).catch( error => {
  console.log(error);

  }
  );






    this.formdata.imagefile = this.selectedFile;
    console.log($event);
   this.data.fileselected='yes';
    this.readThis($event.target);

  }

  requirement(requirementt) {
       
    this.studentlistloaded='1';

      var link = this.global.serverurl+'/news/single.php'; 
      
       var myData = JSON.stringify({id:requirementt});
     
      this.http.post(link,myData)
      //this.http.get(link)    
       .subscribe(data => {
    
       this.req = data["_body"];
    //alert(this.data);
            console.log(this.req);

       this.req = JSON.parse(this.req);
    if(this.req.status=='success'){
      this.req=this.req.records[0];
      //this.req = [...this.req.records];

      this.studentlistloaded='1';
      this.modal.show();

     
    }
    
       }, error => {
    
       });
       }


  studentlist() {
       
    this.studentlistloaded='0';

      var link = this.global.serverurl+'/banner/view.php'; 
      
       var myData = JSON.stringify({id:this.userid});
     
      this.http.post(link,myData)
      //this.http.get(link)    
       .subscribe(data => {
    
       this.studentlistdata = data["_body"];
    //alert(this.data);
            console.log(this.studentlistdata);

       this.studentlistdata = JSON.parse(this.studentlistdata);
    if(this.studentlistdata.status=='success'){
      this.studentlistrowsFilter=this.studentlistdata.records;
      this.studentlisttempFilter = [...this.studentlistdata.records];
      this.studentlistloaded='1';
   
     
    }
    
       }, error => {
    
       });
       }
      

  ngOnInit() {
    this.storage.get('userdata').then((userdata) => {
     
      
  this.priority.push({label:'High',value:'2'}); 
  this.priority.push({label:'Medium',value:'1'}); 
  this.priority.push({label:'Low',value:'0'}); 
  this.addformdata.priority='1';
  console.log(this.priority)
  this.addformdata.companyid=this.userid;

          this.studentlist();
         
          
  
        
        
      
  
      
      
    });


    setTimeout(() => {
      
      this.editorContent += 'greater pleasures, or else he endures pains to avoid worse pain.';
    }, 2800);

    setTimeout(() => {
      this.profitChartOption = {
        tooltip: {
          trigger: 'item',
          formatter: function(params) {
            const date = new Date(params.value[0]);
            let data = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ';
            data += date.getHours() + ':' + date.getMinutes();
            return data + '<br/>' + params.value[1] + ', ' + params.value[2];
          },
          responsive: true
        },
        dataZoom: {
          show: true,
          start: 70
        },
        legend: {
          data: ['Profit']
        },
        grid: {
          y2: 80
        },
        xAxis: [{
          type: 'time',
          splitNumber: 10
        }],
        yAxis: [{
          type: 'value'
        }],
        series: [{
          name: 'Profit',
          type: 'line',
          showAllSymbol: true,
          symbolSize: function(value) {
            return Math.round(value[2] / 10) + 2;
          },
          data: (function() {
            const d: any = [];
            let len = 0;
            const now = new Date();
            while (len++ < 200) {
              const random1: any = (Math.random() * 30).toFixed(2);
              const random2: any = (Math.random() * 100).toFixed(2);
              d.push([ new Date(2014, 9, 1, 0, len * 10000), random1 - 0, random2 - 0 ]);
            }
            return d;
          })()
        }]
      };
    }, 1);
  }

  updateFilter(event) {
    // alert('ravi');
     const val = event.target.value.toLowerCase();
 console.log(val);
     // filter our data
     const temp = this.tempFilter.filter(function(d) {
       return d.name.toLowerCase().indexOf(val) !== -1 || !val;
     });
 console.log(temp);
     // update the rows
     this.rowsFilter = temp;
     // Whenever the filter changes, always go back to the first page
     this.table.offset = 0;
   }

  updateFilterstudentlistbyname(event) {
    // alert('ravi');
     const val = event.target.value.toLowerCase();
 console.log(val);
     // filter our data
     const temp = this.studentlisttempFilter.filter(function(d) {
       return d.cname.toLowerCase().indexOf(val) !== -1 || !val;
     });
 console.log(temp);
     // update the rows
     this.studentlistrowsFilter = temp;
     // Whenever the filter changes, always go back to the first page
     this.table.offset = 0;
   }
   onActivate(event) {
    if(event.type == 'click') {
        console.log(event.row);
        this.studentlistloaded='1';

        
         this.formdata=event.row;
         console.log(this.formdata);
      
         //this.req = [...this.req.records];
   
         this.studentlistloaded='1';
         this.modal.show();
    }
}

  updateFilterstudentlistbyschoolname(event) {
    // alert('ravi');
     const val = event.target.value.toLowerCase();
 console.log(val);
     // filter our data
     const temp = this.studentlisttempFilter.filter(function(d) {
       return d.projectname.toLowerCase().indexOf(val) !== -1 || !val;
     });
 console.log(temp);
     // update the rows
     this.studentlistrowsFilter = temp;
     // Whenever the filter changes, always go back to the first page
     this.table.offset = 0;
   }
  status(status) {
    // alert('ravi');
     const val = status.toLowerCase();
 console.log(val);
     // filter our data
     const temp = this.studentlisttempFilter.filter(function(d) {
       return d.type.toLowerCase().indexOf(val) !== -1 || !val;
     });
 console.log(temp);
     // update the rows
     this.studentlistrowsFilter = temp;
     // Whenever the filter changes, always go back to the first page
     this.table.offset = 0;
   }

   

  fetchContactData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', 'assets/data/data.json');

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  toggleEditProfile() {
    this.editProfileIcon = (this.editProfileIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
    this.editProfile = !this.editProfile;
  }

  toggleEditAbout() {
    this.editAboutIcon = (this.editAboutIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
    this.editAbout = !this.editAbout;
  }
///////////////////////////
openMyModal(event) {
  document.querySelector('#' + event).classList.add('md-show');
}

closeMyModal(event) {
  ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
}

openBasicModal(event) {
  this.showDialog = !this.showDialog;
  setTimeout(() => {
    document.querySelector('#' + event).classList.add('md-show');
  }, 25);
}

closeBasicModal(event) {
  ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  setTimeout(() => {
    this.visible = false;
    this.showDialog = !this.showDialog;
  }, 300);
}

openSwal() {
  swal({
    title: 'Error!',
    text: 'Do you want to continue',
    type: 'error',
    confirmButtonText: 'Cool',
    allowOutsideClick: true
  }).catch(swal.noop);
}

openBasicSwal() {
  swal({
    title: 'Heres a message!',
    text: 'Its pretty, isnt it?'
  }).catch(swal.noop);
}

openSuccessSwal() {
  swal({
    title: 'Good job!',
    text: 'You clicked the button!',
    type: 'success'
  }).catch(swal.noop);
}

openConfirmsSwal() {
  swal({
    title: 'Are you sure?',
    text: 'You wont be able to revert',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then(function () {
    swal(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    );
  }).catch(swal.noop);
}

openSuccessCancelSwal() {
  swal({
    title: 'Request Closed',
    text: 'Response updated and request closed successfully',
    type: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
   
    confirmButtonText: 'Okay',
    
    confirmButtonClass: 'btn btn-success m-r-10',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false
  }).then(function () {
    swal(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    );
  }, function (dismiss) {
    if (dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Your imaginary file is safe :)',
        'error'
      );
    }
  }).catch(swal.noop);
}

delete(){
  
}
openPromptSwal() {
  swal.setDefaults({
    input: 'text',
    confirmButtonText: 'Next &rarr;',
    showCancelButton: true,
    animation: false,
    progressSteps: ['1', '2', '3']
  });

  const steps = [
    {
      title: 'Question 1',
      text: 'Chaining swal2 modals is easy'
    },
    'Question 2',
    'Question 3'
  ];

  swal.queue(steps).then(function (result) {
    swal.resetDefaults();
    swal({
      title: 'All done!',
      html:
      'Your answers: <pre>' +
      JSON.stringify(result) +
      '</pre>',
      confirmButtonText: 'Lovely!',
      showCancelButton: false
    });
  }, function () {
    swal.resetDefaults();
  }).catch(swal.noop);
}

openAjaxSwal() {
  swal({
    title: 'Submit email to run ajax request',
    input: 'email',
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    preConfirm: function (email) {
      return new Promise(function (resolve, reject) {
        setTimeout(function() {
          if (email === 'taken@example.com') {
            reject('This email is already taken.');
          } else {
            resolve();
          }
        }, 2000);
      });
    },
    allowOutsideClick: false
  }).then(function (email) {
    swal({
      type: 'success',
      title: 'Ajax request finished!',
      html: 'Submitted email: ' + email
    });
  }).catch(swal.noop);
}




 

}

