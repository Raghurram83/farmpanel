import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    main: [
     
     
      {
        state: 'category',
        short_label: 'D',
        name: 'Category',
        type: 'link',
        icon: 'fa fa-list',
         
        
      },
      {
        state: 'subcategory',
        short_label: 'D',
        name: 'subcategory',
        type: 'link',
        icon: 'fa fa-shopping-basket',
         
        
      },
      {
        state: 'product',
        short_label: 'D',
        name: 'Product',
        type: 'link',
        icon: 'fa fa-shopping-bag',
         
        
      },
     
      {
        state: 'banner',
        short_label: 'D',
        name: 'Banner',
        type: 'link',
        icon: 'fa fa-image',
         
        
      },
     
      
    ],
  },
 //////////////// 
 

  {
    label: 'Support',
    main: [
    
      {
        state: 'submit-issue',
        short_label: 'S',
        external: 'http://lab.onstep.in/',
        name: 'Submit Issue',
        type: 'external',
        icon: 'icon-layout-list-post',
        target: true
      }
    ]
  }

];


const TRAINERMENUITEMS = [
  {
    label: 'Navigation',
    main: [
     
      {
        state: 'dashboard',
        short_label: 'D',
        name: 'Dashboard',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
      {
        state: 'records',
        short_label: 'D',
        name: 'Records',
        type: 'sub',
        icon: 'fa fa-heartbeat',
        children: [
         
         
          {
            state: 'viewschools',
            name: 'Add Records'
          },
          {
            state: 'managerecords',
            name: 'Manage Records'
          },
       
          
        ]
      },
     
    

    ],
  },
  {
    label: 'Support',
    main: [
    
      {
        state: 'submit-issue',
        short_label: 'S',
        external: 'http://lab.onstep.in/',
        name: 'Submit Issue',
        type: 'external',
        icon: 'icon-layout-list-post',
        target: true
      }
    ]
  }

];

const DOCTORMENUITEMS = [
  {
    label: 'Navigation',
    main: [
     
      {
        state: 'mycalendar',
        short_label: 'D',
        name: 'Calendar',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
      {
        state: 'patientlist',
        short_label: 'D',
        name: 'Patients',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
     
     
     
     
      {
        state: 'myprofile',
        short_label: 'D',
        name: 'Profile',
        type: 'link',
        icon: 'fa fa-user-md',
     
      },

    ],
  },
 

];
const PATIENTMENUITEMS = [
  {
    label: 'Navigation',
    main: [
     
      {
        state: 'home',
        short_label: 'D',
        name: 'Home',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
      {
        state: 'newrequest',
        short_label: 'D',
        name: 'New request',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
      {
        state: 'myrequests',
        short_label: 'D',
        name: 'Requests',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
      {
        state: 'myappointments',
        short_label: 'D',
        name: 'Appointments',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
      {
        state: 'myaccount',
        short_label: 'D',
        name: 'My Profile',
        type: 'link',
        icon: 'fa fa-tachometer',
        
     
      },
     
     
     
     
    

    ],
  },
 

];


@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
  gettrainer(): Menu[] {
    return TRAINERMENUITEMS;
  }
  getdoctor(): Menu[] {
    return DOCTORMENUITEMS;
  }
  getpatient(): Menu[] {
    return PATIENTMENUITEMS;
  }
}
