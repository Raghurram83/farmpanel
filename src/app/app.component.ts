import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Mega Able 5+';

  constructor(private router: Router, public storage: Storage) { }

  ngOnInit() {
   // alert();
   this.storage.get('loggedin').then((userid) => {
    if(userid){
     // this.router.navigate(['/dashboard']);

    }else{
      this.router.navigate(['/login']);

    }
  
  });
   this.router.events.subscribe((evt) => {
    if (!(evt instanceof NavigationEnd)) {
    return;
    }
   window.scrollTo(0, 0);
  });
  }
}
