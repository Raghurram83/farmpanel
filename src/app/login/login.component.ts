import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { NgForm } from '@angular/forms';  
import {Http, Headers } from '@angular/http';
import swal from 'sweetalert2';
import { FileuploadService } from '../fileupload.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { Storage } from '@ionic/storage';
import PNotify from "pnotify/dist/es/PNotify";
import PNotifyButtons from "pnotify/dist/es/PNotifyButtons";
import PNotifyConfirm from "pnotify/dist/es/PNotifyConfirm";
import {Input, ViewEncapsulation} from '@angular/core';
import { ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {animate, style, transition, trigger} from '@angular/animations';
import { GlobalService } from '../global.service';
@Component({
  selector: 'app-login', 
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss',
  '../../../node_modules/sweetalert2/src/sweetalert2.scss',
  '../../assets/icon/icofont/css/icofont.scss',
  '../../../node_modules/ng2-toasty/style-bootstrap.css',
  '../../../node_modules/ng2-toasty/style-default.css',
  '../../../node_modules/ng2-toasty/style-material.css',
  '../../../node_modules/pnotify/dist/PNotifyBrightTheme.css',
  '../../assets/login/css/main.css',
  '../../assets/login/css/util.css',
  '../../assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
  '../../assets/login/vendor/animate/animate.css',
  '../../assets/login/vendor/css-hamburgers/hamburgers.min.css',
  '../../assets/login/vendor/select2/select2.min.css'

],
encapsulation: ViewEncapsulation.None,
animations: [
  trigger('fadeInOutTranslate', [
    transition(':enter', [
      style({opacity: 0}),
      animate('400ms ease-in-out', style({opacity: 1}))
    ]),
    transition(':leave', [
      style({transform: 'translate(0)'}),
      animate('400ms ease-in-out', style({opacity: 0}))
    ])
  ])
]
})
export class LoginComponent implements OnInit {
  formdata:any={};
  data:any={};
  //data:any;
  image:any;
  loaded:any;
  dumy:any={};
  selectedFile: any;
  imagepreview: any;
  trainerid:any;
  
  old:any={};
  s3details:any;
  upload:any;
  constructor(private route: ActivatedRoute,public global:GlobalService,  private router: Router,public http: Http,private fileupload: FileuploadService, public storage: Storage) { }
  
  
  ngOnInit() {

        document.querySelector('body').setAttribute('themebg-pattern', 'theme5');

    this.formdata={};
    this.data={};
    
    this.storage.get('loggedin').then((data) => {
console.log(data);
//alert(data);

      if(data){
     //  this.router.navigate(['/login']);
  this.router.navigate(['/']); 
     // window.location.reload();
      }else{
      
     
      }
    
    });
    
   
  }
  
  
 
   
  
  
 
    login() {
         
  this.loaded='1';
  
    var link = this.global.serverurl+'/login/login.php'; 
    
     var myData = JSON.stringify({email:  this.formdata.email, password: this.formdata.password});
     console.log(myData);
     
    
     
     
     
    this.http.post(link,myData)
    //this.http.get(link)
    
     .subscribe(data => {
      console.log(data);
  
     this.data = data["_body"];
  //alert(this.data);
  console.log(this.data);
  
     this.data = JSON.parse(this.data);
  console.log(this.data);
  if(this.data.status=='success'){
    if(this.data.records!='wrong'){
      PNotify.success('Login sucessfull.');
        this.storage.set('loggedin', 'yes');
 

  
         this.router.navigate(['/']);
      // window.location.reload();

    }else{

    
     

    }
    if(this.data.records=='wrong'){
      PNotify.error('Oops! Incorrect email or password.');
    }
   
    }
   

   // alert('sucess');
    console.log(data);
  
  
  
     }, error => {
  this.data.status='failed';
     this.loaded='0';
  
     });
     } 
  
     openSuccessCancelSwal() {
      swal({
        title: 'Registerd successfully',
        type: 'success',
        showCancelButton: true,
        cancelButtonText: 'Okay',
  
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#11c15b',
        confirmButtonText: 'View profile'
      }).then((result) => {
        if (result.value) {
          this.router.navigate(['/trainers/trainerprofile/'+this.trainerid]);
  
        }
      })
    }
  
  
  }
  