import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {

  constructor( private route: ActivatedRoute,  private router: Router,  )  { 

  }

  ngOnInit() 
  {
    let id = this.route.snapshot.paramMap.get('id');
alert(id);
  }

}
